import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";

const Home: NextPage = () => {
  console.log(process.env.NAME);
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div
        style={{
          textAlign: "center",
          justifyContent: "center",
          marginTop: "50%",
        }}
      >
        Halo, nama saya {process.env.NEXT_PUBLIC_NAME}
      </div>
    </div>
  );
};

export default Home;
